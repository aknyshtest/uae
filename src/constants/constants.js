export const API_PATH = "https://api.uaecopts.com";
// export const API_PATH = "http://localhost:1338";

export const MODALS_ID = {
  LITURGY_BOOKING_MODAL: "LITURGY_BOOKING_MODAL",
  RESEND_EMAIL_POPUP: "RESEND_EMAIL_POPUP",
  FORGOT_PASSWORD_POPUP: "FORGOT_PASSWORD_POPUP",
  BOOKING_MODIFY_MODAL: "BOOKING_MODIFY_MODAL",
  LITURGY_BOOKED_MODAL: "LITURGY_BOOKED_MODAL",
  DISCLAIMER_MODAL: "DISCLAIMER_MODAL",
};

export const STORAGE_TYPE = {
  LOCAL_STORAGE: "localStorage",
  SESSION_STORAGE: "sessionStorage"
};

export const FAMILY_TYPE = {
  family: "family",
  alone: "alone"
};

export const DEACON_RADIO = [
  { value: true, label: "FamilyPage.Form.Deacon.Yes" },
  { value: false, label: "FamilyPage.Form.Deacon.No" }
];

export const FAMILY_STATUSES = [
  { value: "single", label: "FamilyPage.Form.SocialStatus.Single" },
  { value: "married", label: "FamilyPage.Form.SocialStatus.Married" },
  { value: "widow", label: "FamilyPage.Form.SocialStatus.Widow" },
  { value: "other", label: "FamilyPage.Form.SocialStatus.Other" }
];

export const VISA_TYPES = [
  {
    value: "residence",
    label: "FamilyPage.Form.VisaType.Residence"
  },
  {
    value: "visit",
    label: "FamilyPage.Form.VisaType.Visit"
  },
  {
    value: "other",
    label: "FamilyPage.Form.VisaType.Other"
  }
];

export const VISA_TYPES_VALUES = {
  residence: "residence",
  visit: "visit",
  other: "other"
};

export const NATIONALITIES = [
  { value: "other", label: "FamilyPage.Form.Nationality.Other" },
  { value: "egyptian", label: "FamilyPage.Form.Nationality.Egyptian" }
];

export const NATIONALITIES_VALUES = {
  other: "other",
  egyptian: "egyptian"
};

export const SPOUSE_STATUS = [
  {
    value: "available",
    label: "FamilyPage.Spouse.SpouseStatus.Available"
  },
  {
    value: "passedAway",
    label: "FamilyPage.Spouse.SpouseStatus.PassedAway"
  },
  {
    value: "notAvailable",
    label: "FamilyPage.Spouse.SpouseStatus.NotAvailable"
  }
];

export const SPOUSE_STATUS_VALUES = {
  available: "available",
  passedAway: "passedAway",
  notAvailable: "notAvailable"
};

export const FORMS_NAME = {
  MAIN_MEMBER: "mainMember",
  SPOUSE: "spouse",
  CHILDREN: "children",
  PARENTS: "grandParents"
};

export const FILES_TYPE = {
  PROFILE_PIC: "profilePic",
  NATIONAL_ID: "nationalIDCopy",
  EMIRATES_ID: "emiratesIDCopy"
};

export const LOADERS = {
  bookingEvent: "bookingEvent"
};
