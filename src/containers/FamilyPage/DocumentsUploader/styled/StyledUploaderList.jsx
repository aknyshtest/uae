import styled from 'styled-components'
const StyledUploaderList = styled.div`
	position: relative;
	
    img {
      width: 80px;
      height: 80px;
      object-fit: contain;
      border-radius: 10px; 
    }
  
`

export default StyledUploaderList


