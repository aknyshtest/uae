import styled from 'styled-components'


const StyledUploaderImgBox = styled.div`
    width: 100%;
    overflow: hidden;
`

export default StyledUploaderImgBox;


