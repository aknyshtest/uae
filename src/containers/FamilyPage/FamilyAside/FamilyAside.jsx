import React from 'react';
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { withRouter } from "react-router";
import isNull from 'lodash/isNull'

import { urlLocations } from "../../../routes/urlLocations";
import { familySelector } from "../../../services/family/family.selector";
import { FAMILY_TYPE, SPOUSE_STATUS_VALUES} from "../../../constants/constants";
import StyledAsideItem from '../../styled/StyledAsideItem';
import StyledFamilyAsideMenu from './styled/StyledFamilyAsideMenu';
import StyledFamilyAside from './styled/StyledFamilyAside';
import StyledFamilyAsideLeftIcon from './styled/StyledFamilyAsideLeftIcon';
import StyledAsideIconCheck from './styled/StyledAsideIconCheck';

const FamilyAside = ({
    family: {
        address,
        type,
        mainMember,
        spouse,
        spouseStatus,
        numberOfChildren,
        numberOfGrantParents
    },
    location,
	onCollapse
}) => {
    return(
        <StyledFamilyAside>
            <StyledFamilyAsideMenu>

                <StyledAsideItem
	                    onClick={()=> onCollapse()}
	                    isvalid={Boolean(mainMember)}
                                 type="primary"
                                 to={urlLocations.personalInfo}
                                 isactive={(location.pathname === urlLocations.personalInfo) ? 'true' : 'false' }>
	                    <StyledFamilyAsideLeftIcon>
		                    <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="m20 2h-16c-1.10457 0-2 .89543-2 2v16c0 1.1046.89543 2 2 2h16c1.1046 0 2-.8954 2-2v-16c0-1.10457-.8954-2-2-2zm-16-1c-1.65685 0-3 1.34315-3 3v16c0 1.6569 1.34315 3 3 3h16c1.6569 0 3-1.3431 3-3v-16c0-1.65685-1.3431-3-3-3zm8.0033 5.74609c-1.5282 0-2.76014 1.2277-2.76014 2.73354 0 1.50267 1.22664 2.72827 2.75044 2.73347h.0097.0097c1.5238-.0052 2.7505-1.2308 2.7505-2.73347 0-1.50584-1.2319-2.73354-2.7602-2.73354zm2.0629 5.85541c1.0215-.6667 1.6973-1.8145 1.6973-3.12187 0-2.06583-1.6874-3.73354-3.7602-3.73354-2.07279 0-3.76014 1.66771-3.76014 3.73354 0 1.30717.67561 2.45497 1.69699 3.12167-2.08193.8161-3.56088 2.8305-3.56088 5.194v.5h11.24153v-.5c0-2.3628-1.4745-4.3774-3.5546-5.1938zm-2.0732.6116c.0035.0001.0069.0001.0103.0001s.0069 0 .0103-.0001c2.3776.0052 4.3295 1.7911 4.58 4.0822h-9.18707c.25121-2.2904 2.20814-4.077 4.58647-4.0822z" fill="#9fb4d1" fillRule="evenodd"/></svg></StyledFamilyAsideLeftIcon>
                        <FormattedMessage id="FamilyPage.Aside.PersonalInfo" />
                        { mainMember && <StyledAsideIconCheck /> }

	                {}
                </StyledAsideItem>

                { FAMILY_TYPE.family === type &&
	                <>
	                    <StyledAsideItem  onClick={()=> onCollapse()} isvalid={Boolean(spouse ||
	                    (!spouse &&  spouseStatus &&  SPOUSE_STATUS_VALUES.available !== spouseStatus))}
	                                     type="primary"
	                                     to={urlLocations.spouseInfo}
	                                     isactive={(location.pathname === urlLocations.spouseInfo) ? 'true' : 'false' }>
		                        <StyledFamilyAsideLeftIcon>
			                        <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="m4 2h16c1.1046 0 2 .89543 2 2v16c0 1.1046-.8954 2-2 2h-16c-1.10457 0-2-.8954-2-2v-16c0-1.10457.89543-2 2-2zm-3 2c0-1.65685 1.34315-3 3-3h16c1.6569 0 3 1.34315 3 3v16c0 1.6569-1.3431 3-3 3h-16c-1.65685 0-3-1.3431-3-3zm6.97063 3.07344c.98822-.74116 1.91962-1.31303 2.87707-1.53831.93-.21884 1.9222-.11908 3.0657.56701 1.0955.65734 2.0365 2.13906 2.3926 3.86005.3202 1.54771.1478 3.19531-.7239 4.49611-.5264-.5065-1.1522-.9115-1.8452-1.1835 1.0215-.6667 1.6972-1.8145 1.6972-3.1218 0-.97846-.2641-1.85477-.8336-2.49255-.5767-.64592-1.4132-.98711-2.4298-.98711-.3509 0-.6525.09942-.8828.33215-.2032.20529-.3011.4703-.3671.66226-.017.04953-.0333.09832-.0493.14646l-.0001.00036c-.1206.36182-.2288.68627-.5506 1.0016-.38801.38007-.76596.53372-1.12675.68039l-.08836.03605c-.17762.07295-.41734.17332-.60244.34431-.22035.20358-.3326.47188-.3326.79638 0 1.0124.3457 1.8387.99923 2.4044.10272.0889.21151.17.32581.2436-.62297.2602-1.18926.6287-1.67463 1.0819-.54882-1.1168-.91194-2.5388-.9633-3.874-.06077-1.58015.31708-2.85892 1.11287-3.45576zm9.31467 2.68615c.3762 1.81841.1675 3.86481-1.0293 5.47801.6519.9113 1.0355 2.0259 1.0355 3.2311v.5h-11.24145v-.5c0-1.22.39401-2.3469 1.06157-3.264-.72369-1.3065-1.19135-3.0312-1.25312-4.6371-.06422-1.66979.30792-3.391 1.51213-4.29416 1.01178-.75883 2.08039-1.43699 3.24797-1.71173 1.195-.28118 2.4528-.13096 3.8093.68294 1.4044.84267 2.4634 2.61094 2.8574 4.51494zm-5.6114 4.12691h-.0112c-2.37787.0057-4.33422 1.7921-4.5854 4.0822h9.1871c-.2505-2.2912-2.2025-4.0771-4.5801-4.0822zm2.7602-3.7335c0 1.5026-1.2266 2.7283-2.7504 2.7335h-.0096-.0115c-.8145-.0019-1.4316-.2129-1.83828-.5649-.39798-.3445-.65367-.8748-.65367-1.6483 0-.0229.00182-.0378.00339-.0464l.00118-.0056.00084-.0032.00038-.0012.00287-.003.0025-.0024c.03365-.0311.10643-.0728.3038-.1539.02919-.012.05985-.0243.09187-.0373.36604-.1477.90952-.3671 1.44312-.88979.5053-.49506.6856-1.04799.8037-1.41025.0149-.0457.0288-.08837.0422-.1275.0666-.19368.1057-.25713.1322-.28389.0065-.00661.0131-.0118.0287-.01751.019-.00694.0617-.01802.1433-.01802.7839 0 1.3291.25571 1.6839.65314.3621.40557.5795 1.01908.5795 1.82652z" fill="#9fb4d1" fillRule="evenodd"/></svg>
		                        </StyledFamilyAsideLeftIcon>
		                        <FormattedMessage id="FamilyPage.Aside.SpouseInfo" />
	                            { (spouse ||
	                                (!spouse &&  spouseStatus &&  SPOUSE_STATUS_VALUES.available !== spouseStatus))
	                            && <StyledAsideIconCheck isactive={(location.pathname === urlLocations.spouseInfo) ? 'true' : 'false' } />}
	                    </StyledAsideItem>

	                    <StyledAsideItem
		                    onClick={()=> onCollapse()}
		                    isvalid={!isNull(numberOfChildren)}
		                    type="primary"
		                    to={urlLocations.childrenList}
		                    isactive={(location.pathname === urlLocations.childrenList) ? 'true' : 'false' }>
		                        <StyledFamilyAsideLeftIcon>
			                        <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><g clipRule="evenodd" fill="#9fb4d1" fillRule="evenodd"><path d="m15.7158 8.89255c.5717-.5782 1.4687-.50271 1.961.1254.464.59187.4302 1.49275-.1133 2.03135-.0001.0002.0002-.0002 0 0l-1.9304 1.9215c-.5094.5049-.8106 1.2415-.8106 2.0332v3.0282c0 .7536-.5566 1.4677-1.3611 1.4677h-.0891c-.8045 0-1.3611-.7141-1.3611-1.4677v-.9773c0-.0443-.016-.0744-.0289-.0889-.0005-.0005-.0009-.001-.0013-.0015-.0005.0005-.0009.001-.0014.0015-.0129.0145-.0289.0446-.0289.0889v.9773c0 .7536-.5566 1.4677-1.3611 1.4677h-.0891c-.80447 0-1.36107-.7141-1.36107-1.4677v-3.0663c0-.7873-.30152-1.5286-.81065-2.0332l-.00074-.0008-1.89094-1.882c-.00019-.0002.00019.0002 0 0-.54341-.5386-.57781-1.44008-.11384-2.03195.49566-.63231 1.39193-.69458 1.95972-.12665l2.18812 2.1731c.4295.4273.9754.6556 1.5311.6556.5556 0 1.1012-.228 1.5307-.6553zm-3.7264 8.06485c0 .0001-.0004.0003-.0011.0006zm-.0158.0006c-.0007-.0003-.0011-.0005-.0011-.0006zm4.9162-7.32312c-.1349-.17204-.3308-.17338-.4634-.03868l-.0036.00362-2.1846 2.17378c-.6055.6024-1.4004.9464-2.236.9464s-1.6305-.3439-2.23596-.9463c-.00004 0 .00004.0001 0 0l-2.18863-2.17356-.00156-.00156c-.1364-.13666-.33389-.13194-.46578.0363-.16366.20879-.1383.53732.03038.70432l.00095.001 1.89114 1.8822c.00012.0002-.00013-.0001 0 0 .7123.7062 1.10666 1.7099 1.10666 2.7435v3.0663c0 .3142.2155.4677.3611.4677h.0891c.1457 0 .3611-.1535.3611-.4677v-.9773c0-.5486.4081-1.0959 1.0303-1.0959.6221 0 1.0302.5473 1.0302 1.0959v.9773c0 .3142.2154.4677.3611.4677h.0891c.1457 0 .3611-.1535.3611-.4677v-3.0282c0-1.0387.3943-2.0372 1.1064-2.7431.0001-.0001-.0001.0001 0 0l1.9296-1.9207.0009-.001c.1687-.167.1941-.49553.0304-.70432z"/><path d="m12 5.5c-1.1046 0-2 .89543-2 2s.8954 2 2 2 2-.89543 2-2-.8954-2-2-2zm-3 2c0-1.65685 1.3431-3 3-3s3 1.34315 3 3-1.3431 3-3 3-3-1.34315-3-3z"/><path d="m20 2h-16c-1.10457 0-2 .89543-2 2v16c0 1.1046.89543 2 2 2h16c1.1046 0 2-.8954 2-2v-16c0-1.10457-.8954-2-2-2zm-16-1c-1.65685 0-3 1.34315-3 3v16c0 1.6569 1.34315 3 3 3h16c1.6569 0 3-1.3431 3-3v-16c0-1.65685-1.3431-3-3-3z"/></g></svg>
		                        </StyledFamilyAsideLeftIcon>
		                        <FormattedMessage id="FamilyPage.Aside.ChildrenInfo" />
	                            { !isNull(numberOfChildren) && <StyledAsideIconCheck isactive={(location.pathname === urlLocations.childrenList) ? 'true' : 'false' } />  }
	                    </StyledAsideItem>
	                </>
                }
	            <StyledAsideItem
		            onClick={()=> onCollapse()}
		            isvalid={!isNull(numberOfGrantParents)}
		            type="primary"
		            to={urlLocations.parentsList}
		            isactive={(location.pathname === urlLocations.parentsList) ? 'true' : 'false' }>
		                <StyledFamilyAsideLeftIcon><svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="m20 2h-16c-1.10457 0-2 .89543-2 2v16c0 1.1046.89543 2 2 2h16c1.1046 0 2-.8954 2-2v-16c0-1.10457-.8954-2-2-2zm-16-1c-1.65685 0-3 1.34315-3 3v16c0 1.6569 1.34315 3 3 3h16c1.6569 0 3-1.3431 3-3v-16c0-1.65685-1.3431-3-3-3zm8.2148 8.26363c0-.98065.7821-1.76363 1.7322-1.76363s1.7322.78298 1.7322 1.76363c0 .98067-.7821 1.76367-1.7322 1.76367s-1.7322-.783-1.7322-1.76367zm-3.15634 3.43097c-.00853.0001-.01708.0002-.02563.0002-.00857 0-.01713-.0001-.02567-.0002-.59808.003-1.02946.1111-1.34496.2766-.31458.165-.54216.4018-.71148.7091-.3034.5506-.41731 1.3186-.4441 2.2634h10.43908c-.0278-1.1308-.1617-2.0625-.5321-2.731-.2037-.3677-.4782-.6532-.856-.8517-.3829-.2013-.901-.3283-1.6106-.3283-1.3521 0-2.0297.4521-2.4189 1.0941l-.3181.5247-.4497-.4176c-.3362-.3122-.84797-.5349-1.70184-.5393zm1.65194-.6791c.099.0453.1937.0952.2843.1499.3057-.3522.6874-.6288 1.1546-.8205-.2922-.2586-.5293-.5792-.6915-.9409-.042.631-.3201 1.1989-.7474 1.6115zm5.0336-.6699c.574-.5077.9352-1.2539.9352-2.08197 0-1.51969-1.2167-2.76363-2.7322-2.76363-1.4801 0-2.6752 1.18658-2.7302 2.65775-.3942-.81428-1.22099-1.37845-2.18397-1.37845-1.34892 0-2.43054 1.10698-2.43054 2.4577 0 .6952.28646 1.3257.74838 1.7741-.05212.0235-.10314.0484-.15306.0746-.50501.2649-.86838.6504-1.12273 1.112-.49208.8931-.57488 2.0749-.57488 3.246v.5h12.4516v-.5c0-1.3566-.1024-2.7033-.6632-3.7156-.2885-.5208-.6982-.9541-1.2655-1.2523-.0897-.0472-.1827-.0906-.2789-.1302zm-6.68985.349c-.00707 0-.01415 0-.02124 0-.00716 0-.0143 0-.02143 0-.77386-.0116-1.40919-.6533-1.40919-1.4576 0-.81167.64705-1.4577 1.43054-1.4577.78348 0 1.43057.64603 1.43057 1.4577 0 .8044-.63538 1.446-1.40925 1.4576z" fill="#9fb4d1" fillRule="evenodd"/></svg></StyledFamilyAsideLeftIcon>
		                <FormattedMessage id="FamilyPage.Aside.ParentsInfo" />
	                    { !isNull(numberOfGrantParents) && <StyledAsideIconCheck isactive={(location.pathname === urlLocations.parentsList) ? 'true' : 'false' } />}
	            </StyledAsideItem>

	            <StyledAsideItem
		            onClick={()=> onCollapse()}
		            isvalid={Boolean(address)}
		            type="primary"
		            to={urlLocations.addressInfo}
		            isactive={(location.pathname === urlLocations.addressInfo) ? 'true' : 'false' }>
		                <StyledFamilyAsideLeftIcon><svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="m20 2h-16c-1.10457 0-2 .89543-2 2v16c0 1.1046.89543 2 2 2h16c1.1046 0 2-.8954 2-2v-16c0-1.10457-.8954-2-2-2zm-16-1c-1.65685 0-3 1.34315-3 3v16c0 1.6569 1.34315 3 3 3h16c1.6569 0 3-1.3431 3-3v-16c0-1.65685-1.3431-3-3-3zm12.2889 11.5553c.467-1.0537.7111-1.9385.7111-2.5553 0-2.76142-2.2386-5-5-5-2.76142 0-5 2.23858-5 5 0 .6168.24408 1.5016.7111 2.5553.45632 1.0295 1.08289 2.1268 1.74881 3.137.66609 1.0104 1.35509 1.9086 1.92479 2.5431.2425.27.4503.4776.6153.6222.165-.1446.3728-.3522.6153-.6222.5697-.6345 1.2587-1.5327 1.9248-2.5431.6659-1.0102 1.2925-2.1075 1.7488-3.137zm-4.5344 6.4903c-.0121.0065-.011.0047.0018-.001zm.4892-.001c.0128.0057.0139.0075.0018.001zm-.2437.9554c1 0 6-6.6863 6-10 0-3.31371-2.6863-6-6-6-3.31371 0-6 2.68629-6 6 0 3.3137 5 10 6 10zm0-8c1.1046 0 2-.8954 2-2 0-1.10457-.8954-2-2-2s-2 .89543-2 2c0 1.1046.8954 2 2 2z" fill="#9fb4d1" fillRule="evenodd"/></svg></StyledFamilyAsideLeftIcon>
		                <FormattedMessage id="FamilyPage.Aside.AddressInfo" />
	                    { address && <StyledAsideIconCheck isactive={(location.pathname === urlLocations.addressInfo) ? 'true' : 'false' } />}
	            </StyledAsideItem>

            </StyledFamilyAsideMenu>
        </StyledFamilyAside>
    )
};

export default compose(
    connect(
        createStructuredSelector({
            family: familySelector
        }),{}
    ),
    withRouter,
)(FamilyAside);