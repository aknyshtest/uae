import styled from 'styled-components'
import { DEVICE } from '../../../constants/media';

const StyledPageWrapSubTitle = styled.div`
   position: relative;
   width: 100%;
   margin-top: 38px;
   margin-bottom: 5px;
   
    @media ${DEVICE.mobileDevices} {
		margin-bottom: 20px;
	} 
	  
  &:after {
    content: '';
    position:absolute;
    right: 0;
    top: 14px;
    width: 100%;
    height: 1px;
    background: var(--lightGrey);
  }
  
`
export default StyledPageWrapSubTitle;


