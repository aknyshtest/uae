import styled from 'styled-components'
import { DEVICE } from '../../../../constants/media';

const StyledFormCol33 = styled.div`
	  width: 100%;
	  display: flex;
	  
	  @media ${DEVICE.mobileDevices} {
		flex-wrap: wrap;
	  } 
`
export default StyledFormCol33 ;


