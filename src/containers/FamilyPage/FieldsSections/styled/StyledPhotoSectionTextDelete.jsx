import styled from 'styled-components'

const StyledPhotoSectionTextDelete = styled.div`
  font-size: 13px;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.3;
  letter-spacing: normal;
  color:var(--dark);
`
export default StyledPhotoSectionTextDelete;


