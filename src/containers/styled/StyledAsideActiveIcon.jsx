import styled from 'styled-components'
import ArrowHref from '../../assets/icons/arrowWhite.svg';

const StyledAsideActiveIcon = styled.i`
	position: absolute;
	width: 24px;
	height: 24px;
	right: 20px;
    top: 17px;
    transform: rotate(-90deg);
    display: ${isactive  => isactive ? 'none;' : 'none;'};
	background: url(${ArrowHref}) center center no-repeat;
`
export default StyledAsideActiveIcon;


