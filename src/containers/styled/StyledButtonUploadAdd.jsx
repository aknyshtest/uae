import React from 'react';
import styled, { css } from 'styled-components';
import { Button } from 'antd';
import { DEVICE } from '../../constants/media';
import plusIcon from '../../assets/icons/add.svg';

const StyledButtonUploadAdd = styled.div`
        display: block;
		width: 80px;
		height: 80px;
		border-radius: 8px;
		margin-right: 24px;
		color: var(--hrefColorSecondary);
		border: 1px solid var(--hrefColorSecondary);
		display: flex;
		justify-content: center;
		align-items: center;
		box-shadow: 0 10px 15px 0 rgba(66, 101, 218, 0.1);
		box-shadow: 0 10px 20px 0 rgba(66, 101, 218, 0.2);
		transition: all 0.3 ease-in;
		background: var(--bgLight) url(${plusIcon}) center center no-repeat;
		margin-right: 24px;
		&:hover {
		     box-shadow: 0 10px 30px 0 rgba(66, 101, 218, 0.3);
		 }
		&:hover {
		     box-shadow: 0 10px 30px 0 rgba(66, 101, 218, 0.2);
		    &:disabled {
		         color: var(--dark);
		         border: 1px solid #d6e1e9;
		     }
		 }
		 
		&:disabled {
		     color: var(--dark);
		     border: 1px solid #d6e1e9;  }
		     

 
    
`
export default StyledButtonUploadAdd;
