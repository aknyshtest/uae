import styled from 'styled-components';

const StyledInfoBoxTxt = styled.span`
  font-family: var(--fontProximaBold);
  font-size: 14px;
  line-height: 1.45;
  color: #27282b;
  padding-left: 4px;
`
export default StyledInfoBoxTxt;