import styled from 'styled-components';

const StyledError = styled.div`
  font-size: 10px;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: normal;
  color: var(--red);
  padding-top: 5px;
`
export default StyledError;
