import styled from 'styled-components';

const StyledLiturgyBookingMembers = styled.div`
  margin-bottom: 17px;
  width: 80%;
  justify-content: space-between;
  display: flex;
  align-items: center;
`
export default StyledLiturgyBookingMembers;