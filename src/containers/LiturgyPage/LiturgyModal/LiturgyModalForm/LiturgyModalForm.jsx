import React from 'react';
import { connect } from "react-redux";
import { compose } from "redux";
import { reduxForm } from "redux-form";
import { createStructuredSelector } from 'reselect';
import isArray from "lodash/isArray";
import {
    LoadingOutlined,
} from '@ant-design/icons';

import CheckboxField from "../../../../components/CheckboxField/CheckboxField";
import { familyMembersSelector } from '../../../../services/family/family.selector';
import {prepareMemberIds} from "../../../../services/events/events.utils";
import {eventAvailabilitySelector} from "../../../../services/eventAvailability/eventAvailability.selector";
import {getLocalizedKey} from "../../../../utils/utils";
import {bookEventAction} from "../../../../services/bookings/bookings.action";
import {getBookingByEventIdSelector} from "../../../../services/bookings/bookings.selector";
import {FormattedMessage} from "react-intl";
import StyledModalFooter from './styled/StyledModalFooter';
import StyledModalButtonBook from './styled/StyledModalButtonBook';
import StyledModalButtonClose from './styled/StyledModalButtonClose';
import StyledLiturgyBookingMembers from './styled/StyledLiturgyBookingMembers';
import StyledModalForm from './styled/StyledModalForm';
import StyledLiturgyBookingReason from './styled/StyledLiturgyBookingReason';
import { setModalStatusAction } from "../../../../services/modals/modals.action";
import {isLoadingSelector} from "../../../../services/loader/loader.selector";
import {LOADERS} from "../../../../constants/constants";


const LiturgyModalForm = ({ handleSubmit, setModalStatus, members, availability, booking, isLoading }) => {
    const isButtonDisabled =
        !Object.values(availability)?.some(member =>  member && member?.canBook) || isLoading;

    const bookedMembers = isArray(booking?.bookedMembers) ? booking?.bookedMembers : [booking?.bookedMembers];

    return (
        <StyledModalForm>
            <form onSubmit={handleSubmit}>
            {
                members?.map((member) => (
                    member?.id && <StyledLiturgyBookingMembers key={member?.id}>
                    <CheckboxField
                        defaultChecked={bookedMembers?.some(
                            bookedMember => bookedMember &&  bookedMember?.toString() === member.id?.toString()

                        )}
                        disabled={!availability[member?.id]?.canBook}
                        name={`bookedMembers."${member?.id}"`}
                    >
                        {member?.firstName}
                    </CheckboxField>

                    <StyledLiturgyBookingReason>
                        { availability[member?.id]?.[getLocalizedKey('reason')] }
                    </StyledLiturgyBookingReason>

	            </StyledLiturgyBookingMembers>
                ))
            }

                <StyledModalFooter>
                    <StyledModalButtonClose onClick={() => setModalStatus(null)}>
                        <FormattedMessage
                            id="LiturgyPage.Modal.Close"
                        />
                    </StyledModalButtonClose>

                    <StyledModalButtonBook
                        disabled={isButtonDisabled}
                        htmlType="submit"
                    >
                        { isLoading && <><LoadingOutlined/>{" "}</> }
                        <FormattedMessage
                            id="LiturgyPage.Modal.BookNow"
                        />
                    </StyledModalButtonBook>

            </StyledModalFooter>
            </form>
        </StyledModalForm>
    )
};

export default compose(
    connect(
        createStructuredSelector({
            members: familyMembersSelector,
            availability: eventAvailabilitySelector,
            booking: (state, { eventId }) => getBookingByEventIdSelector(state, eventId),
            isLoading: (state) => isLoadingSelector(state, LOADERS.bookingEvent)
        }),
        {
            setModalStatus: setModalStatusAction
        }
    ),
    reduxForm({
        form: "liturgyForm",
        onSubmit: (value, dispatch, { eventId }) =>
            dispatch(bookEventAction({
                event: eventId,
                bookedMembers: prepareMemberIds(value.bookedMembers)
            })),
    })
)(LiturgyModalForm)