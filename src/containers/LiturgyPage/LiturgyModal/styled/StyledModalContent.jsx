import styled from 'styled-components';

const StyledModalContent = styled.div`
  padding: 0 0 90px 24px;
`
export default StyledModalContent;