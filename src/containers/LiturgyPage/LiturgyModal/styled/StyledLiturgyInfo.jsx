import styled from 'styled-components';

const StyledLiturgyInfo = styled.div`
  font-size: 11px;
  line-height: 1.45;
  color: var(--gray);
  display: inline-flex;
  align-items: center;
  text-transform: uppercase;
  margin-right: 32px;
  margin-bottom: 18px; 
  width: ${({ fullwidth })  => (fullwidth) && '100%;'};
  
  
`
export default StyledLiturgyInfo;