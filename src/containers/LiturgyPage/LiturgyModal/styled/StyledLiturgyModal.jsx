import styled from 'styled-components';

const StyledLiturgyModal = styled.div`
  width: 688px;
  height: 506px;
  border-radius: 10px;
  box-shadow: 0 0 10px 0 rgba(139, 155, 163, 0.5);
  border: solid 1px #d6e1e9;
  background-color: #ffffff;
`
export default StyledLiturgyModal;