import styled from 'styled-components';

const StyledLiturgyInfoTxt = styled.span`
  font-family: var(--fontProximaBold);
  font-size: 11px;
  line-height: 1.45;
  color: #27282b;
`
export default StyledLiturgyInfoTxt;