import styled from 'styled-components';

const StyledLiturgyAsideImg = styled.img`
	width: 48px;
	height: 48px;
	border-radius: 50%;
	box-shadow: 3px 3px 4px 0 rgba(0, 0, 0, 0.25);
	margin-right: 16px;
`
export default StyledLiturgyAsideImg;