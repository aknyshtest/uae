import styled from 'styled-components';

const StyledLiturgyItemHeader = styled.div`
  padding-bottom: 10px;
  padding-left: 32px;
  background: var(--white);
`
export default StyledLiturgyItemHeader;