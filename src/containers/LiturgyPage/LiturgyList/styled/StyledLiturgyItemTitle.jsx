import styled from 'styled-components';

const StyledLiturgyItemTitle = styled.div`
  font-size: 17px;
  font-weight: bold;
  font-family: var(--fontProximaBold);
  line-height: 1.5;
  padding: 24px 10px 16px 0;
`
export default StyledLiturgyItemTitle;