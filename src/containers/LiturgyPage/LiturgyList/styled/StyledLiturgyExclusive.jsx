import styled from 'styled-components';

const StyledLiturgyExclusive = styled.div`
  font-size: 13px;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.23;
  letter-spacing: normal;
  color: var(--red);
  padding-top: 5px;
`
export default StyledLiturgyExclusive;