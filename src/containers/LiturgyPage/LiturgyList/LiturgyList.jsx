import React from 'react';
import { compose } from "redux";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { FormattedMessage } from 'react-intl';

import { getChurchLiturgySelector } from "../../../services/events/events.selector";
import StyledPageTitle from '../../styled/StyledPageTitle';
import StyledLiturgyList from './styled/StyledLiturgyList';
import LiturgyCard from "../LiturgyCard/LiturgyCard";

const LiturgyList = ({
    liturgy,
    selectEvent,
}) => {
    const sortedLiturgy = liturgy.sort((a, b) =>
        new Date(a.date) - new Date(b.date));

    return (
        <div>
            <StyledPageTitle>
                <FormattedMessage
                    id="LiturgyPage.Title"
                />
            </StyledPageTitle>
            <StyledLiturgyList>
            {
                sortedLiturgy.length ? sortedLiturgy.map(item => (
                    item &&  <LiturgyCard
                        key={item.id}
                        eventId={item.id}
                        item={item}
                        selectEvent={selectEvent}
                    />
                )) : <h2><FormattedMessage id={"LiturgyPage.NoLiturgy"} /></h2>
            }
            </StyledLiturgyList>
        </div>
    )
};

export default compose(
    withRouter,
    connect(
        createStructuredSelector({
            liturgy: (state, {  match : { params: { id } }}) =>
                getChurchLiturgySelector(state, id),
        }),

    )
)(LiturgyList)