import styled from 'styled-components'
import { DEVICE } from '../../../../constants/media';

const StyledBookingPageImg = styled.div`
    width: 207px;

	@media ${DEVICE.mobileDevices} {
		width: 100%;
	}
`

export default StyledBookingPageImg;


