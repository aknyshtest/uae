import styled from "styled-components";

const Content = styled.div`
  padding: 25px 0 75px;
`;

export default {
  Content
};
