import styled from 'styled-components';
import { DEVICE } from '../../../constants/media';

const StyledDashboardItem = styled.div`
	width: 100%;
	width: 360px;
	max-width: 360px;
    max-height: 368px;
    height: 368px;
    border-radius: 10px;
	margin-left: 22px;
	margin-right: 22px;
    border: solid 1px var(--lightGrey);
    box-shadow: 0 0 10px 0 rgba(139, 155, 163, 0.1);
    overflow: hidden;
    position: relative;
    background-image: var(--${(props) => props.color});
    
    &:hover {
         box-shadow: 0 10px 20px 0 rgba(139, 155, 163, 0.2);
    }
   
   
	@media ${DEVICE.tabletDevices1200} {
		width: 300px;
		margin-bottom: 45px;
		
	}
	
	@media ${DEVICE.mobileDevicesS} {
		width: 290px;
		margin-bottom: 45px;
	}
	
	@media ${DEVICE.laptopL} {
		height: 368px;
		width: 360px;
	}
   
    
    &:after {   
        content: '';
        position: absolute;
		right: 10;
		top: 80px;
		width: 200px;
		height: 200px;
		border-radius: 200px;
		background: #fff;
		z-index: -1;
    }
    
    &>a {
    	display: flex;
		justify-content: center;
		flex-wrap: wrap;
    }
`
export default StyledDashboardItem;