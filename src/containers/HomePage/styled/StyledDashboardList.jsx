import styled from 'styled-components';
import { DEVICE } from '../../../constants/media';

const StyledDashboardList = styled.div`
	display: flex;
	margin: 120px -22px 0 -22px;
	max-width: 1200px;
	justify-content: space-between;
	
	
	
	@media ${DEVICE.mobileDevices} {
		flex-wrap: wrap;
		justify-content: center;
		
	}
	@media (min-width: 768px) and (max-width: 1199px) {
		flex-wrap: wrap;
		justify-content: start;
		margin: 80px auto 0 30px;
	}

`
export default StyledDashboardList;