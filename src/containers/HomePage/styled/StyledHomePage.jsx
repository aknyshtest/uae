import styled from 'styled-components';

const StyledHomePage = styled.div`
	background: var(--bgLight);
	position: relative;
`
export default StyledHomePage;