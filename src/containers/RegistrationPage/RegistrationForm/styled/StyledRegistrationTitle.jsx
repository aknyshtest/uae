import styled from 'styled-components';

const StyledRegisterTitle = styled.h2`
	text-align: center;
    color: var(--dark);
    padding-bottom: 30px;
    font-size: 24px;
	font-weight: bold;
	font-stretch: normal;
	font-style: normal;
	line-height: 1.33;
	letter-spacing: normal;
`
export default StyledRegisterTitle;