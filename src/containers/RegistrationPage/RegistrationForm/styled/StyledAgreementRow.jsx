import styled from 'styled-components'

const StyledAgreementRow = styled.div`
	margin-top: 24px;
	margin-bottom: 24px;
`;

export default StyledAgreementRow;