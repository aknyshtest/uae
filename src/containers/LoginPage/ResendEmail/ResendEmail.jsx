import React from 'react';

import ResendEmailModal from "./ResendEmail.Popup/ResendEmail.Popup";

const ResendEmail = () =>  <ResendEmailModal/>;

export default ResendEmail;