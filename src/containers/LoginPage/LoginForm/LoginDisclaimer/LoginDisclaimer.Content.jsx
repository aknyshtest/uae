import React from 'react';
import { connect } from 'react-redux'
import { submit } from 'redux-form'

import { FormattedMessage } from "react-intl";
import Styled from './LoginDesclaimer.styled';

const LoginDisclaimerContent = ({ setPopupStatus, dispatch }) => {
    return (
        <Styled.Content>
            <h1><FormattedMessage id="Disclaimer.Title" /></h1>
            <Styled.Desc>
                <FormattedMessage id="Disclaimer.Desc" />
            </Styled.Desc>
            <Styled.Button onClick={() => { dispatch(submit("loginForm")) }} >Accept</Styled.Button>
            <Styled.Button uiType="secondary" onClick={() => setPopupStatus(null)}  >Close</Styled.Button>
        </Styled.Content>
    );
};

export default connect()(LoginDisclaimerContent);