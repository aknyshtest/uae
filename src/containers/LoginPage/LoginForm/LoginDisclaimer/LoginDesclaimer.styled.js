import styled from 'styled-components';
import Button from '../../../../components/Button/Button';


const Content = styled.div`
  padding: 20px;
`;

const Desc = styled.div`
  margin-bottom: 20px;
`;

const StyledButton = styled(Button)`
  &&& {
    display: inline-block;
    margin-right: 15px;
  }
`;

export default {
    Content,
    Desc,
    Button: StyledButton
}