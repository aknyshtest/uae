import React from 'react';
import LoginDisclaimerModal from './LoginDisclaimer.Modal';

const LoginDisclaimer = () => {
    return <LoginDisclaimerModal />
};

export default LoginDisclaimer;