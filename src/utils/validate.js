export const passwordValidate = values => {
  const passwordValidation = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
  const passwordErrorMessage = "Password.Validation";
  const errors = {};
  if (values.password && !passwordValidation.test(values.password)) {
    errors.password = passwordErrorMessage;
  }
  if (
    values.passwordConfirmation &&
    !passwordValidation.test(values.passwordConfirmation)
  ) {
    errors.passwordConfirmation = passwordErrorMessage;
  }

  return errors;
};
