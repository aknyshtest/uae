const lang = localStorage.getItem("lang");

export const getLocalizedKey = filedName =>
  lang === "en" ? filedName : `${filedName}Ar`;

export const disableFutureDate = current =>
  current && current.valueOf() > Date.now();
