import styled from 'styled-components'
import { DEVICE } from '../../../constants/media';

const StyledLogo = styled.div`
	  width: 140px;
	  height:40px;
	  margin-left: 40px;
`
export default StyledLogo;