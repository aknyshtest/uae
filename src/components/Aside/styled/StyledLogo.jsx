import styled from 'styled-components'

const StyledLogo = styled.div`
	  width: 140px;
	  height:40px;
	  margin-left: 40px;
`
export default StyledLogo;