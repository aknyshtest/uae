import styled from 'styled-components'


const StyledLangControl = styled.div`
    {
        position: absolute; 
        top: 40px;
        right: 40px;
        width: 167px; 
        height: 40px;
		border-radius: 10px;
	    border: 1px solid  #d6e1e9;
    }
`

export default StyledLangControl;


