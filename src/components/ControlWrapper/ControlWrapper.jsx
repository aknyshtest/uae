import React from 'react';
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import StyledError from "./styled/StyledError";

const ControlWrapper = ({ label, children, name, errors, error, customTouched, touched }) =>  (
    <>
        { label && <label><FormattedMessage id={label} /></label> }
        { children }
        {
            errors?.[name] && !customTouched && errors?.[name].map(error =>
                <StyledError key={error}>
	                <FormattedMessage key={error} id={ error } />
                </StyledError>
            )

        }
        {
            error && touched &&
                <StyledError key={error}>
                    <FormattedMessage key={error} id={ error } />
                </StyledError>
        }
    </>
);

export default connect(
    ({ errors }) => ({ errors }),
    {}
)(ControlWrapper)