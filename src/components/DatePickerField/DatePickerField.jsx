import React, { useState, useEffect } from 'react';
import DatePicker  from '../../components/DatePicker/DatePicker';
import { Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import moment from 'moment';
import suffixIconDatepicker from '../../assets/icons/Calendar.svg';

import ControlWrapper from "../ControlWrapper/ControlWrapper";

const CustomDatePicker = ({
    input: { value, name, ...restInput },
    formatMessage,
    setTouched,
    disabledDate,
    customTouched,
    label,
    placeholder,
    ...meta
}) => {
    return <ControlWrapper
        label={label}
        name={name}
        customTouched={customTouched}
    >
        <DatePicker
            suffixIcon={<img src={suffixIconDatepicker} alt={""} />}
            { ...restInput }
            {...meta}
            onFocus={setTouched.bind(null, true)}
            placeholder={
                placeholder && formatMessage({ id: placeholder })
            }
            inputReadOnly
            selected={value ? moment(value, "YYYY-MM-DD") : moment(Date.now())}
            value={value ? moment(value, "YYYY-MM-DD") : moment(Date.now())}
            disabledDate={disabledDate}
        />

    </ControlWrapper>
};

const DatePickerField = ({
    name,
    errors,
    intl: { formatMessage },
    placeholder,
    label,
    disabledDate
}) => {
    const [ customTouched, setTouched ] = useState(false);
    useEffect(() => {
        setTouched(false)
    }, [errors]);
    return (
        <Field
            component={CustomDatePicker}
            name={name}
            formatMessage={formatMessage}
            placeholder={placeholder}
            label={label}
            disabledDate={disabledDate}
            customTouched={customTouched}
            setTouched={setTouched}
        />
    )};

export default compose(
    connect(
        ({ errors }) => ({ errors }),
        {}),
    injectIntl
)(DatePickerField);