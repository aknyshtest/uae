import { FILES_UPLOAD, FILE_UPLOAD_SUCCESS, CLEAR_FILES } from "./file.action";
import initialState from "../../store/initialState";
import { FILES_TYPE } from "../../constants/constants";

export default (state = initialState.files, { type, payload }) => {
  switch (type) {
    case FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        [payload.formName]: {
          ...state[payload.formName],
          [payload.name]:
            payload.name === FILES_TYPE.PROFILE_PIC
              ? [...payload.files]
              : [...state?.[payload.formName]?.[payload.name], ...payload.files]
        }
      };
    case FILES_UPLOAD:
      return {
        ...state,
        [payload.formName]: {
          ...payload.files
        }
      };
    case CLEAR_FILES:
      return {
        ...state,
        [payload.formName]: initialState.files[payload.formName]
      };
    default:
      return state;
  }
};
