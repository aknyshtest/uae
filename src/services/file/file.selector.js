export const selectFile = (state, name, formName) =>
  state.files?.[formName]?.[name];
