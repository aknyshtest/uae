export const familySelector = state => state?.family;
export const familyMembersSelector = state => [
  state?.family.mainMember,
  state?.family.spouse,
  ...state?.family.children,
  ...state?.family.grandParents
];
