import { call, put, takeEvery } from "redux-saga/effects";

import {
  CHECK_EVENT_AVAILABILITY,
  CHECK_EVENT_AVAILABILITY_FAILURE,
  CHECK_EVENT_AVAILABILITY_SUCCESS
} from "./eventAvailability.action";

import { checkAvailabilityRequest } from "./eventAvailability.api";

function* checkEventAvailability({ payload }) {
  try {
    const { response } = yield call(checkAvailabilityRequest, { ...payload });

    if (response.status === 200) {
      yield put({
        type: CHECK_EVENT_AVAILABILITY_SUCCESS,
        payload: response.data
      });
    } else {
      yield put({
        type: CHECK_EVENT_AVAILABILITY_FAILURE
      });
    }
  } catch (error) {
    yield put({ type: CHECK_EVENT_AVAILABILITY_FAILURE });
    console.log(error);
  }
}

export default function* eventAvailabilitySaga() {
  yield takeEvery(CHECK_EVENT_AVAILABILITY, checkEventAvailability);
}
