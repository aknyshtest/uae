import instance from "../root.api";

export const checkAvailabilityRequest = ({ id, query = "" }) =>
  instance
    .get(`/events/${id}/availability${query}`)
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
