import { CHECK_EVENT_AVAILABILITY_SUCCESS } from "./eventAvailability.action";
import initialState from "../../store/initialState";

export default (state = initialState.eventAvailability, action) => {
  switch (action.type) {
    case CHECK_EVENT_AVAILABILITY_SUCCESS:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
