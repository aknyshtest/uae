export const eventAvailabilitySelector = state =>
  Object.values(state.eventAvailability)?.reduce((acc, member) => {
    return { ...acc, ...{ [member.id]: member } };
  }, {});
