import instance from "../root.api";

export const setFamilySpouseRequest = data =>
  instance
    .put("/families/me/spouse", { ...data })
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
