import { createAction } from "redux-actions";

export const SET_FAMILY_SPOUSE = "SET_FAMILY_SPOUSE";
export const SET_FAMILY_SPOUSE_SUCCESS = "SET_FAMILY_SPOUSE_SUCCESS";
export const SET_FAMILY_SPOUSE_FAILURE = "SET_FAMILY_SPOUSE_FAILURE";
export const setFamilySpouseAction = createAction(SET_FAMILY_SPOUSE);
