export const familyChildrenSelector = state => state.family?.children;

export const familyChildSelector = (state, id) =>
  state.family?.children.find(child => child.id.toString() === id);
