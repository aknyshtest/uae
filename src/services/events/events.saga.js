import { call, put, select, takeEvery } from "redux-saga/effects";
import { LOCATION_CHANGE } from "connected-react-router";
import isEmpty from "lodash/isEmpty";

import {
  GET_EVENTS,
  GET_EVENTS_FAILURE,
  GET_EVENTS_SUCCESS
} from "./events.action";

import { eventsRequest } from "./events.api";
import { isUrlMatch } from "../router/router.utils";
import { urlLocations } from "../../routes/urlLocations";

function* getEventsOnLocationChangeData({ payload }) {
  try {
    const events = yield select(state => state.events);
    if (isEmpty(events) && isUrlMatch(payload, urlLocations.liturgy))
      yield put({ type: GET_EVENTS });
  } catch (error) {
    console.log(error);
  }
}

function* fetchEvents() {
  try {
    const { response } = yield call(eventsRequest);

    if (response.status === 200) {
      yield put({
        type: GET_EVENTS_SUCCESS,
        payload: response.data
      });
    } else {
      yield put({
        type: GET_EVENTS_FAILURE
      });
    }
  } catch (error) {
    yield put({ type: GET_EVENTS_FAILURE });
    console.log(error);
  }
}

export default function* eventsSaga() {
  yield takeEvery(LOCATION_CHANGE, getEventsOnLocationChangeData);
  yield takeEvery(GET_EVENTS, fetchEvents);
}
