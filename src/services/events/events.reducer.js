import { GET_EVENTS_SUCCESS } from "./events.action";
import initialState from "../../store/initialState";

export default (state = initialState.events, action) => {
  switch (action.type) {
    case GET_EVENTS_SUCCESS:
      return {
        ...action.payload
      };
    default:
      return state;
  }
};
