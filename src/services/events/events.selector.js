export const getChurchLiturgySelector = (state, id) => {
  return Object.values(state?.events).filter(
    event => event.church?.id.toString() === id
  );
};

export const getLiturgiesSelector = state => Object.values(state?.events);
