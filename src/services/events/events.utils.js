export const prepareMemberIds = (ids = {}) => {
  const checked = ids && Object.entries(ids).filter(id => id && !!id[1]);
  return (
    checked &&
    Object.values(checked).map(
      id => id && parseInt(id[0].replace(/['"]+/g, ""))
    )
  );
};
