export const familyParentsSelector = state => state.family?.grandParents;
export const familyParentSelector = (state, id) =>
  state.family?.grandParents.find(parent => parent.id.toString() === id);
