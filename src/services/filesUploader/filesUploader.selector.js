export const isFileUploadingSelector = (state, formName, name) =>
  state?.fileUploader?.[formName]?.[name]?.isUploading;
