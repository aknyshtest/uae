import { createAction } from "redux-actions";

export const SET_GEOLOCATION = "SET_GEOLOCATION";

export const setGeolocationAction = createAction(SET_GEOLOCATION);
