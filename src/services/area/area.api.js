import instance from "../root.api";

export const areasRequest = id =>
  instance
    .get(`/areas?emirate=${id}&_limit=-1`)
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
