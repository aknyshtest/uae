import { createAction } from "redux-actions";

export const GET_AREAS = "GET_AREAS";
export const GET_AREAS_SUCCESS = "GET_AREAS_SUCCESS";
export const GET_AREAS_FAILURE = "GET_AREAS_FAILURE";

export const getAreasAction = createAction(GET_AREAS);
