export const areasSelector = state =>
  state?.areas && Object.values(state.areas);
