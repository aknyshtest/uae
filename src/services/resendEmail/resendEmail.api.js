import instance from "../root.api";

export const resendEmailRequest = ({ email }) =>
  instance
    .post(`/auth/send-email-confirmation`, { email })
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
