import { createAction } from "redux-actions";

export const SET_FAMILY_MAIN_MEMBER = "SET_FAMILY_MAIN_MEMBER";
export const SET_FAMILY_MAIN_MEMBER_SUCCESS = "SET_FAMILY_MAIN_MEMBER_SUCCESS";
export const SET_FAMILY_MAIN_MEMBER_FAILURE = "SET_FAMILY_MAIN_MEMBER_FAILURE";
export const setFamilyMainMember = createAction(SET_FAMILY_MAIN_MEMBER);
