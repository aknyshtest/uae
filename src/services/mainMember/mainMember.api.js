import instance from "../root.api";

export const setFamilyMainMemberRequest = data =>
  instance
    .put("/families/me/main-member", { ...data })
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
