import instance from "../root.api";

export const appInfoRequest = () =>
  instance
    .get("/info")
    .then(response => ({ response }))
    .catch(response => ({ ...response }));
