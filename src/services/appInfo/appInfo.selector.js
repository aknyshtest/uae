export const appInfoChurchesSelector = state => state.appInfo.churches;
export const appInfoFathersSelector = state => state.appInfo.fathers;
export const appInfoWorkingFieldsSelector = state =>
  state.appInfo.workingFields;
export const appInfoEmiratesSelector = state => state.appInfo?.emirates;
