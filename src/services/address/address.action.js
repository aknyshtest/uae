import { createAction } from "redux-actions";

export const SET_FAMILY_ADDRESS = "SET_FAMILY_ADDRESS";
export const SET_FAMILY_ADDRESS_SUCCESS = "SET_FAMILY_ADDRESS_SUCCESS";
export const SET_FAMILY_ADDRESS_FAILURE = "SET_FAMILY_ADDRESS_FAILURE";
export const setFamilyAddressAction = createAction(SET_FAMILY_ADDRESS);
