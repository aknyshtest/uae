import styled from 'styled-components';

const StyledAccordionLink = styled.a`
   display: inline-block;
   margin-left: 8px;
`
export default StyledAccordionLink;