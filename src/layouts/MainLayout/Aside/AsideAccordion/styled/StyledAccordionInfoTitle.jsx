import styled from 'styled-components';

const StyledAccordionInfo = styled.div`
  padding-bottom: 8px;
`
export default StyledAccordionInfo;