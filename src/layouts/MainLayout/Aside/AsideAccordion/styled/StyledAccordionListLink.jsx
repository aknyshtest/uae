import styled from 'styled-components';

const StyledAccordionListLink = styled.div`
   display: flex;
   position: absolute;
   right: 24px;
   bottom: 22px;
`
export default StyledAccordionListLink;