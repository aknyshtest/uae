import React from 'react';
import AuthorizeLayout from "../AuthorizeLayout";

const LiturgyLayout = ({ children }) =>
    <AuthorizeLayout>{ children }</AuthorizeLayout>;

export default LiturgyLayout;