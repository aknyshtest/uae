import styled from 'styled-components';
import bodyBg from '../../../assets/img/bgbodytext.png';

const StyledAutorizeLayout = styled.div`
	position: relative;
	background: var(--bgLight);
	min-height: 100vh;
	overflow-x: hidden;
	
	&:after {	
		content: '';
		position: absolute;
		width: 1100px;
		height: 157px;
		bottom: 0;
		left: 334px;
		overflow: hidden;
		z-index: 2;
		background: url(${bodyBg}) center center no-repeat;	
			
	}
  
`
export default StyledAutorizeLayout;
