import React from 'react';
import AuthorizeLayout from "../AuthorizeLayout";

const FamilyLayout = ({ children }) =>
    <AuthorizeLayout>{ children }</AuthorizeLayout>;

export default FamilyLayout;