import React from 'react';
import AuthorizeLayout from "../AuthorizeLayout";

const BookingLayout = ({ children }) =>
    <AuthorizeLayout>{ children }</AuthorizeLayout>;

export default BookingLayout;